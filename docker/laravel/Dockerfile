FROM php:7.4-cli

ARG USER_ID=1000

RUN useradd -m developer -u ${USER_ID} && \
    mkdir /app && \
    chown developer:developer /app && \
    mkdir -p /home/developer/.composer/cache && \
    chown -R developer:developer /home/developer/.composer

RUN apt update && apt install -y lsb-release wget gnupg2

RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" | \
    tee  /etc/apt/sources.list.d/pgdg.list

RUN apt update && \
    apt install libzip-dev zlib1g-dev libicu-dev libpq-dev libxml2-dev libonig-dev git unzip \
    postgresql-client-13 --no-install-recommends -y && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    apt clean

RUN docker-php-ext-install bcmath pdo_pgsql pgsql zip

RUN pecl install xdebug && docker-php-ext-enable xdebug

ADD docker/laravel/config/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

WORKDIR /app

COPY  --chown=developer ./ ./
COPY  ./docker/laravel/config/php.ini /usr/local/etc/php/php.ini

USER developer