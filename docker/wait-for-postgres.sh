#!/bin/sh
# wait-for-postgres.sh

set -e

cmd="$@"

retries=10

until PGPASSWORD="$DB_PASSWORD" pg_isready -h "$DB_HOST" -U "$DB_USERNAME" > /dev/null 2>&1; do
	retries=$((retries-1))

	if [ $retries -eq 0 ]
	then
		echo "max attempts reached"
		exit 1;
	fi

	 echo "Waiting for postgres server, $retries remaining attempts..."
         sleep 10
	 done

	 echo "Postgres is up - executing command $cmd"
	 exec $cmd