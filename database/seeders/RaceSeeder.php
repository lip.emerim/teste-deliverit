<?php

namespace Database\Seeders;

use App\Models\Enums\RaceTypes;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('races');
        $table->delete();

        $table->insert([
            'type' => RaceTypes::THREE_KM,
            'date' => Carbon::yesterday()->format('Y-m-d'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'type' => RaceTypes::FIVE_KM,
            'date' => Carbon::tomorrow()->format('Y-m-d'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'type' => RaceTypes::TEN_KM,
            'date' => Carbon::tomorrow()->format('Y-m-d'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'type' => RaceTypes::THREE_KM,
            'date' => Carbon::now()->subDays(2)->format('Y-m-d'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'type' => RaceTypes::TWENTY_ONE_KM,
            'date' => Carbon::now()->subDays(3)->format('Y-m-d'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'type' => RaceTypes::FORTY_TWO_KM,
            'date' => Carbon::now()->subDays(4)->format('Y-m-d'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
