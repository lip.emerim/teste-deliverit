<?php

namespace Database\Seeders;

use App\Models\Race;
use App\Models\Runner;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RaceRunnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('races_runners');

        $table->delete();

        /** @var Runner[] $runners */
        $runners = Runner::query()->orderBy('id')->get();
        /** @var Race[] $races */
        $races = Race::query()->orderBy('id')->get();

        $table->insert([
            'race_id' => $races[0]->id,
            'runner_id' => $runners[0]->id,
            'started_at' => Carbon::yesterday()->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::yesterday()->addHour()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[0]->id,
            'runner_id' => $runners[1]->id,
            'started_at' => Carbon::yesterday()->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::yesterday()->addHours(3)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[0]->id,
            'runner_id' => $runners[2]->id,
            'started_at' => Carbon::yesterday()->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::yesterday()->addHours(2)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[1]->id,
            'runner_id' => $runners[0]->id,
            'started_at' => null,
            'finished_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[1]->id,
            'runner_id' => $runners[1]->id,
            'started_at' => null,
            'finished_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);




        $table->insert([
            'race_id' => $races[3]->id,
            'runner_id' => $runners[0]->id,
            'started_at' => Carbon::now()->subDays(2)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::yesterday()->addHour()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[3]->id,
            'runner_id' => $runners[1]->id,
            'started_at' => Carbon::now()->subDays(2)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(2)->addHours(3)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[3]->id,
            'runner_id' => $runners[2]->id,
            'started_at' => Carbon::now()->subDays(2)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(2)->addHours(2)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[4]->id,
            'runner_id' => $runners[0]->id,
            'started_at' => Carbon::now()->subDays(3)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(3)->addHour()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[4]->id,
            'runner_id' => $runners[1]->id,
            'started_at' => Carbon::now()->subDays(3)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(3)->addHours(3)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[4]->id,
            'runner_id' => $runners[2]->id,
            'started_at' => Carbon::now()->subDays(3)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(3)->addHours(2)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[5]->id,
            'runner_id' => $runners[0]->id,
            'started_at' => Carbon::now()->subDays(4)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(4)->addHour()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[5]->id,
            'runner_id' => $runners[1]->id,
            'started_at' => Carbon::now()->subDays(4)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(4)->addHours(3)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'race_id' => $races[5]->id,
            'runner_id' => $runners[2]->id,
            'started_at' => Carbon::now()->subDays(4)->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => Carbon::now()->subDays(4)->addHours(2)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
