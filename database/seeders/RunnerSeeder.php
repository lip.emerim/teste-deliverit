<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RunnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('runners');
        $table->delete();

        $table->insert([
            'name' => 'Barry Allen',
            'cpf' => '39800746099',
            'birth_date' => '1989-03-14',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'name' => 'A-Train',
            'cpf' => '41779725094',
            'birth_date' => '1987-01-14',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $table->insert([
            'name' => 'Wally west',
            'cpf' => '68269366072',
            'birth_date' => '1903-10-11',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
