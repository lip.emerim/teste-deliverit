<?php

use App\Models\Enums\RunnerAgeTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRunnerAgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runner_ages', function (Blueprint $table) {
            $table->id();
            $table->integer('start_age');
            $table->integer('end_age');
            $table->text('name');
        });

        // inserting the data here since this is not seeding but essential data
        $table = DB::table('runner_ages');

        $table->insert([
            'start_age' => 18,
            'end_age' => 24,
            'name' => RunnerAgeTypes::EIGHTEEN_TO_TWENTY_FIVE,
        ]);

        $table->insert([
            'start_age' => 25,
            'end_age' => 34,
            'name' => RunnerAgeTypes::TWENTY_FIVE_TO_THIRTY_FIVE,
        ]);

        $table->insert([
            'start_age' => 35,
            'end_age' => 44,
            'name' => RunnerAgeTypes::THIRTY_FIVE_TO_FORTY_FIVE,
        ]);

        $table->insert([
            'start_age' => 45,
            'end_age' => 54,
            'name' => RunnerAgeTypes::FORTY_FIVE_TO_FIFTY_FIVE,
        ]);

        $table->insert([
            'start_age' => 55,
            'end_age' => 200,
            'name' => RunnerAgeTypes::ABOVE_FIFTY_FIVE,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runner_ages');
    }
}
