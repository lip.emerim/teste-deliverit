<?php

use App\Http\Controllers\RaceController;
use App\Http\Controllers\RaceRunnerController;
use App\Http\Controllers\RunnerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/races-runners', [RaceRunnerController::class, 'addRunnerToRace']);
Route::get('/races-runners', [RaceRunnerController::class, 'list']);
Route::post('/races-runners/{id}/add-results', [RaceRunnerController::class, 'addRunnerResultsToRace']);

Route::resources([
    'runners' => RunnerController::class,
    'races' => RaceController::class,
]);
