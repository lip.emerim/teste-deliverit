<?php

namespace Tests\Unit\Controllers;

use App\Exceptions\ValidationException;
use App\Models\Runner;
use App\Services\RunnerService;
use Mockery\MockInterface;
use Tests\TestCase;

class RunnerControllerTest extends TestCase
{
    protected MockInterface $runnerService;

    public function testStore(): void
    {
        $data = [
            'name' => 'Romero Britto',
            'cpf' => '59963887074',
            'birth_date' => '1998-01-01'
        ];

        $response = new Runner();
        $response->fill($data);

        $this->runnerService->shouldReceive('store')->with($data)->andReturn($response);

        $this->runnerService->shouldReceive('store')->with($data)->andReturn('it works');

        $response = $this->json('POST', '/api/runners', $data);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals($data, $response->json());
    }

    public function testStoreInvalid(): void
    {
        $data = [
            'name' => 'Romero Britto',
            'cpf' => '59963887074',
            'birth_date' => '1998-01-01'
        ];

        $this->runnerService->shouldReceive('store')->with($data)->andThrow(
            new ValidationException(['error' => 'yes'], 'did not work')
        );

        $response = $this->json('POST', '/api/runners', $data);

        $this->assertEquals(422, $response->getStatusCode());

        $expected = [
            'message' => 'did not work',
            'errors' => [
                'error' => 'yes',
            ]
        ];

        $this->assertEquals($expected, $response->json());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->runnerService = $this->mock(RunnerService::class);
    }
}
