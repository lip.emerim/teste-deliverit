<?php

namespace Tests\Unit\Controllers;

use App\Exceptions\ValidationException;
use App\Models\Race;
use App\Services\RaceService;
use Carbon\Carbon;
use Mockery\MockInterface;
use Tests\TestCase;

class RaceControllerTest extends TestCase
{
    protected MockInterface $raceService;

    public function testStore(): void
    {
        $data = [
            'type' => '42_KM',
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
        ];

        $response = new Race();
        $response->fill($data);

        $this->raceService->shouldReceive('store')->with($data)->andReturn($response);

        $response = $this->json('POST', '/api/races', $data);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals($data, $response->json());
    }

    public function testStoreInvalid(): void
    {
        $data = [
            'type' => 'sa',
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
        ];

        $this->raceService->shouldReceive('store')->with($data)->andThrow(
            new ValidationException(['error' => 'yes'], 'did not work')
        );

        $response = $this->json('POST', '/api/races', $data);

        $this->assertEquals(422, $response->getStatusCode());

        $expected = [
            'message' => 'did not work',
            'errors' => [
                'error' => 'yes',
            ]
        ];

        $this->assertEquals($expected, $response->json());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->raceService = $this->mock(RaceService::class);
    }
}
