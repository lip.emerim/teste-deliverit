<?php

namespace Tests\Unit\Controllers;

use App\Exceptions\ValidationException;
use App\Services\RaceRunnerService;
use Mockery\MockInterface;
use Tests\TestCase;

class RaceRunnerControllerTest extends TestCase
{
    protected MockInterface $raceRunnerService;

    public function testAddRunnerToRace()
    {
        $data = [
            'runner_id' => 1,
            'race_id' => 1,
        ];

        $this->raceRunnerService->shouldReceive('addRunnerToRace')
            ->with($data)->andReturn($data);

        $response = $this->json('POST', '/api/races-runners', $data);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals($data, $response->json());
    }

    public function testAddRunnerToRaceInvalid()
    {
        $data = [
            'runner_id' => 1,
            'race_id' => 1,
        ];

        $this->raceRunnerService->shouldReceive('addRunnerToRace')
            ->with($data)->andThrow(
                new ValidationException(['error' => 'yes'], 'did not work')
            );

        $response = $this->json('POST', '/api/races-runners', $data);

        $this->assertEquals(422, $response->getStatusCode());

        $expected = [
            'message' => 'did not work',
            'errors' => [
                'error' => 'yes',
            ]
        ];

        $this->assertEquals($expected, $response->json());
    }

    public function testAddRunnerResultsToRace()
    {
        $data = [
            'started_at' => 1,
            'finished_at' => 1,
        ];

        $this->raceRunnerService->shouldReceive('addRunnerResultsToRace')
            ->with(1, $data)->andReturn($data);

        $response = $this->json('POST', '/api/races-runners/1/add-results', $data);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($data, $response->json());
    }

    public function testAddRunnerResultsToRaceInvalid()
    {
        $data = [
            'started_at' => 1,
            'finished_at' => 1,
        ];

        $this->raceRunnerService->shouldReceive('addRunnerResultsToRace')
            ->with(1, $data)->andThrow(
                new ValidationException(['error' => 'yes'], 'did not work')
            );

        $response = $this->json('POST', '/api/races-runners/1/add-results', $data);

        $this->assertEquals(422, $response->getStatusCode());

        $expected = [
            'message' => 'did not work',
            'errors' => [
                'error' => 'yes',
            ]
        ];

        $this->assertEquals($expected, $response->json());
    }

    public function testList()
    {
        $data = ['yes' => 'it worked'];

        $this->raceRunnerService->shouldReceive('list')
            ->with(RaceRunnerService::GROUP_BY_AGE)->andReturn($data);

        $response = $this->json('GET', '/api/races-runners', [
            'group_by' => RaceRunnerService::GROUP_BY_AGE
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($data, $response->json());
    }

    public function testListInvalid()
    {
        $data = ['yes' => 'it worked'];

        $this->raceRunnerService->shouldReceive('list')
            ->with(RaceRunnerService::GROUP_BY_AGE)->andThrow(
                new ValidationException(['error' => 'yes'], 'did not work')
            );

        $response = $this->json('GET', '/api/races-runners', [
            'group_by' => RaceRunnerService::GROUP_BY_AGE
        ]);

        $this->assertEquals(422, $response->getStatusCode());
        $expected = [
            'message' => 'did not work',
            'errors' => [
                'error' => 'yes',
            ]
        ];

        $this->assertEquals($expected, $response->json());
    }

    public function testListNullGroup()
    {
        $data = ['yes' => 'it worked'];

        $this->raceRunnerService->shouldReceive('list')
            ->with(RaceRunnerService::GROUP_BY_TYPE)->andReturn($data);

        $response = $this->json('GET', '/api/races-runners', [
            'group_by' => null
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($data, $response->json());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->raceRunnerService = $this->mock(RaceRunnerService::class);
    }
}
