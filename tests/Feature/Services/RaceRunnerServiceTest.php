<?php

namespace Tests\Feature\Services;

use App\Exceptions\ValidationException;
use App\Models\Enums\RaceTypes;
use App\Models\Enums\RunnerAgeTypes;
use App\Models\Race;
use App\Models\RaceRunnerByAge;
use App\Models\RaceRunnerByType;
use App\Services\RaceRunnerService;
use Carbon\Carbon;
use Illuminate\Contracts\Container\BindingResolutionException;

class RaceRunnerServiceTest extends BaseServiceTestCase
{
    protected RaceRunnerService $raceRunnerService;

    public function testAddRunnerToRace(): void
    {
        $data = [
            'race_id' => 3,
            'runner_id' => 3,
        ];

        $this->raceRunnerService->addRunnerToRace($data);
        $this->assertDatabaseHas('races_runners', $data);
    }

    public function testAddRunnerToRaceRaceMissing(): void
    {
        $data = [
            'runner_id' => 3,
        ];

        try {
            $this->raceRunnerService->addRunnerToRace($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('race_id', $errors);

            $this->assertEquals(
                'The race id field is required.',
                $errors['race_id'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerToRaceRaceInvalid(): void
    {
        $data = [
            'race_id' => 99,
            'runner_id' => 3,
        ];

        try {
            $this->raceRunnerService->addRunnerToRace($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('race_id', $errors);

            $this->assertEquals(
                'The selected race id is invalid.',
                $errors['race_id'][0]
            );

            $this->assertDatabaseMissing('races_runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerToRaceRunnerMissing(): void
    {
        $data = [
            'race_id' => 3,
        ];

        try {
            $this->raceRunnerService->addRunnerToRace($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('runner_id', $errors);

            $this->assertEquals(
                'The runner id field is required.',
                $errors['runner_id'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerToRaceRunnerInvalid(): void
    {
        $data = [
            'race_id' => 3,
            'runner_id' => 99,
        ];

        try {
            $this->raceRunnerService->addRunnerToRace($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('runner_id', $errors);

            $this->assertEquals(
                'The selected runner id is invalid.',
                $errors['runner_id'][0]
            );

            $this->assertDatabaseMissing('races_runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerToRaceRunnerAlreadyInRace(): void
    {
        $data = [
            'race_id' => 2,
            'runner_id' => 2,
        ];

        try {
            $this->raceRunnerService->addRunnerToRace($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('runner_id', $errors);

            $this->assertEquals(
                'The runner already is in this race.',
                $errors['runner_id'][0]
            );
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerToRaceRunnerScheduleIsBusy(): void
    {
        $data = [
            'race_id' => 3,
            'runner_id' => 2,
        ];

        try {
            $this->raceRunnerService->addRunnerToRace($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('runner_id', $errors);

            $this->assertEquals(
                'The runner already has a race on that date.',
                $errors['runner_id'][0]
            );

            $this->assertDatabaseMissing('races_runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerResultsToRace(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'started_at' => $date->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => $date->addHour()->format('Y-m-d H:i:s'),
        ];

        $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);

        $this->assertDatabaseHas(
            'races_runners',
            [
                'race_id' => 2,
                'runner_id' => 1,
                'started_at' => $data['started_at'],
                'finished_at' => $data['finished_at'],
            ]
        );
    }

    public function testAddRunnerResultsToRaceStartedAtMissing(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'finished_at' => $date->addHour()->format('Y-m-d H:i:s'),
        ];

        try {
            $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('started_at', $errors);

            $this->assertEquals(
                'The started at field is required.',
                $errors['started_at'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerResultsToRaceStartedAtInvalidFormat(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'started_at' => 'sadas',
            'finished_at' => $date->addHour()->format('Y-m-d H:i:s'),
        ];

        try {
            $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('started_at', $errors);

            $this->assertEquals(
                'The started at does not match the format Y-m-d H:i:s.',
                $errors['started_at'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerResultsToRaceStartedAtBeforeRace(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'started_at' => (clone $date)->subSecond()->format('Y-m-d H:i:s'),
            'finished_at' => (clone $date)->addHour()->format('Y-m-d H:i:s'),
        ];

        try {
            $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('started_at', $errors);

            $this->assertEquals(
                "The started at must be a date after ${date}.",
                $errors['started_at'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerResultsToRaceFinishedAtMissing(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'started_at' => $date->format('Y-m-d H:i:s'),
        ];

        try {
            $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('finished_at', $errors);

            $this->assertEquals(
                'The finished at field is required.',
                $errors['finished_at'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerResultsToRaceFinishedAtInvalidFormat(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'started_at' => $date->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => "sadas",
        ];

        try {
            $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('finished_at', $errors);

            $this->assertEquals(
                'The finished at does not match the format Y-m-d H:i:s.',
                $errors['finished_at'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testAddRunnerResultsToRaceFinishedAtBeforeStartedAt(): void
    {
        /** @var Race $race */
        $race = Race::query()->find(2);
        $date = Carbon::parse($race->date);

        $raceRunnerId = 4;

        $data = [
            'started_at' => $date->addMinute()->format('Y-m-d H:i:s'),
            'finished_at' => $date->subSecond()->format('Y-m-d H:i:s'),
        ];

        try {
            $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('finished_at', $errors);

            $this->assertEquals(
                'The finished at must be a date after started at.',
                $errors['finished_at'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testListByAge()
    {
        $data = $this->raceRunnerService->list(RaceRunnerService::GROUP_BY_AGE);

        $this->assertEquals(3, count($data));
        // 3 race types + 12 runners + 6 age types (2 for each race type)
        $this->assertEquals(21, count($data, COUNT_RECURSIVE));

        /** @var RaceRunnerByAge $firstRunner */
        $firstRunner = $data[RaceTypes::THREE_KM][RunnerAgeTypes::TWENTY_FIVE_TO_THIRTY_FIVE][0];

        $this->assertEquals(RaceTypes::THREE_KM, $firstRunner->race_type);
        $this->assertEquals(1, $firstRunner->position);
        $this->assertEquals(1, $firstRunner->runner_id);
        $this->assertEquals('Barry Allen', $firstRunner->runner_name);
        $this->assertEquals(31, $firstRunner->runner_age);
        $this->assertEquals(1, $firstRunner->race_id);
        $this->assertEquals(RunnerAgeTypes::TWENTY_FIVE_TO_THIRTY_FIVE, $firstRunner->age_type);
    }

    public function testListByType()
    {
        $data = $this->raceRunnerService->list(RaceRunnerService::GROUP_BY_TYPE);

        $this->assertEquals(3, count($data));
        // 3 race types + 12 runners
        $this->assertEquals(15, count($data, COUNT_RECURSIVE));

        /** @var RaceRunnerByType $firstRunner */
        $firstRunner = $data[RaceTypes::THREE_KM][0];

        $this->assertEquals(RaceTypes::THREE_KM, $firstRunner->race_type);
        $this->assertEquals(1, $firstRunner->position);
        $this->assertEquals(1, $firstRunner->runner_id);
        $this->assertEquals('Barry Allen', $firstRunner->runner_name);
        $this->assertEquals(31, $firstRunner->runner_age);
        $this->assertEquals(1, $firstRunner->race_id);
    }

    public function testListInvalidGroup()
    {
        try {
            $this->raceRunnerService->list("sadas");
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('group_by', $errors);

            $this->assertEquals(
                'The selected group by is invalid.',
                $errors['group_by'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->raceRunnerService = $this->app->make(RaceRunnerService::class);
    }
}
