<?php

namespace Tests\Feature\Services;

use App\Exceptions\ValidationException;
use App\Models\Enums\RaceTypes;
use App\Services\RaceService;
use Carbon\Carbon;
use Illuminate\Contracts\Container\BindingResolutionException;

class RaceServiceTest extends BaseServiceTestCase
{
    protected RaceService $raceService;

    public function testStore(): void
    {
        $data = [
            'type' => RaceTypes::TEN_KM,
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
        ];

        $this->raceService->store($data);
        $this->assertDatabaseHas('races', $data);
    }

    public function testStoreTypeMissing(): void
    {
        $data = [
            'date' => '2020-01-01',
        ];

        try {
            $this->raceService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('type', $errors);

            $this->assertEquals(
                'The type field is required.',
                $errors['type'][0]
            );

            $this->assertDatabaseMissing('races', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreTypeInvalid(): void
    {
        $data = [
            'type' => 'sa',
            'date' => '2020-01-01',
        ];

        try {
            $this->raceService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('type', $errors);

            $this->assertEquals(
                'The selected type is invalid.',
                $errors['type'][0]
            );

            $this->assertDatabaseMissing('races', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreDateMissing(): void
    {
        $data = [
            'type' => RaceTypes::TWENTY_ONE_KM,
        ];

        try {
            $this->raceService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('date', $errors);

            $this->assertEquals(
                'The date field is required.',
                $errors['date'][0]
            );
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreDateWrongFormat(): void
    {
        $data = [
            'type' => RaceTypes::TWENTY_ONE_KM,
            'date' => 'sa'
        ];

        try {
            $this->raceService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('date', $errors);

            $this->assertEquals(
                'The date does not match the format Y-m-d.',
                $errors['date'][0]
            );

            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreDatePast(): void
    {
        $data = [
            'type' => RaceTypes::TWENTY_ONE_KM,
            'date' => '1998-01-01',
        ];

        try {
            $this->raceService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('date', $errors);

            $this->assertEquals(
                'The date must be a date after or equal to today.',
                $errors['date'][0]
            );

            $this->assertDatabaseMissing('races', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->raceService = $this->app->make(RaceService::class);
    }
}
