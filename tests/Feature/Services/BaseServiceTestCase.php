<?php

namespace Tests\Feature\Services;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BaseServiceTestCase extends TestCase
{
    use RefreshDatabase;

    protected bool $seed = true;
    protected Generator $faker;

    protected function setUp(): void
    {
        $this->faker = Factory::create();
        parent::setUp();
    }
}
