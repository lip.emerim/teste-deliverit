<?php

namespace Tests\Feature\Services;

use App\Exceptions\ValidationException;
use App\Services\RunnerService;
use Carbon\Carbon;
use Illuminate\Contracts\Container\BindingResolutionException;

class RunnerServiceTest extends BaseServiceTestCase
{
    protected RunnerService $runnerService;

    public function testStore(): void
    {
        $data = [
            'name' => 'Romero Britto',
            'cpf' => '59963887074',
            'birth_date' => '1998-01-01'
        ];

        $this->runnerService->store($data);
        $this->assertDatabaseHas('runners', $data);
    }

    public function testStoreNameMissing(): void
    {
        $data = [
            'cpf' => '59963887074',
            'birth_date' => '1998-01-01'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('name', $errors);

            $this->assertEquals(
                'The name field is required.',
                $errors['name'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreNameTooShort(): void
    {
        $data = [
            'name' => 'sa',
            'cpf' => '59963887074',
            'birth_date' => '1998-01-01'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('name', $errors);

            $this->assertEquals(
                'The name must be at least 5 characters.',
                $errors['name'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreNameTooLong(): void
    {
        $length = 256;
        $this->faker->lexify(str_repeat('?', $length));

        $data = [
            'name' => $this->faker->lexify(str_repeat('?', $length)),
            'cpf' => '59963887074',
            'birth_date' => '1998-01-01'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('name', $errors);

            $this->assertEquals(
                'The name may not be greater than 255 characters.',
                $errors['name'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreCpfMissing(): void
    {
        $data = [
            'name' => 'Romero britto',
            'birth_date' => '1998-01-01'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('cpf', $errors);

            $this->assertEquals(
                'The cpf field is required.',
                $errors['cpf'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreCpfInvalid(): void
    {
        $data = [
            'name' => 'Romero britto',
            'cpf' => '1234',
            'birth_date' => '1998-01-01'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('cpf', $errors);

            $this->assertEquals(
                'The cpf field is invalid.',
                $errors['cpf'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreCpfAlreadyTaken(): void
    {
        $data = [
            'name' => 'Romero britto',
            'cpf' => '39800746099',
            'birth_date' => '1998-01-01'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('cpf', $errors);

            $this->assertEquals(
                'The cpf has already been taken.',
                $errors['cpf'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreBirthDateMissing(): void
    {
        $data = [
            'name' => 'Romero britto',
            'cpf' => '59963887074',
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('birth_date', $errors);

            $this->assertEquals(
                'The birth date field is required.',
                $errors['birth_date'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreBirthDateWrongFormat(): void
    {
        $data = [
            'name' => 'Romero britto',
            'cpf' => '59963887074',
            'birth_date' => '01/01/1998'
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('birth_date', $errors);

            $this->assertEquals(
                'The birth date does not match the format Y-m-d.',
                $errors['birth_date'][0]
            );
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    public function testStoreBirthDateTooRecent(): void
    {
        $data = [
            'name' => 'Romero britto',
            'cpf' => '59963887074',
            'birth_date' => Carbon::now()->format('Y-m-d')
        ];

        try {
            $this->runnerService->store($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();

            $this->assertArrayHasKey('birth_date', $errors);

            $minDate = Carbon::now()->subYears(18)->format('Y-m-d');

            $this->assertEquals(
                "The birth date must be a date before ${minDate}.",
                $errors['birth_date'][0]
            );

            $this->assertDatabaseMissing('runners', $data);
            return;
        }

        $this->fail("Expected exception not thrown");
    }

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->runnerService = $this->app->make(RunnerService::class);
    }
}
