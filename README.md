<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

 [![pipeline status](https://gitlab.com/lip.emerim/teste-deliverit/badges/master/pipeline.svg)](https://gitlab.com/lip.emerim/teste-deliverit/-/commits/master) 

## Instalation

### Setup app

Copy .env file

`cp .env.example .env`

### Build images

`docker-compose build`

### Install packages

`docker-compose run --rm laravel composer install`

### Generate app key

`docker-compose run --rm laravel php artisan key:generate`

### Running app

`docker-compose up`

### Migrations and seeding

With the app is already running, run:

`docker-compose run --rm laravel php artisan migrate:refresh`   
`docker-compose run --rm laravel php artisan db:seed`

### Running tests

With the app is already running, run:

`docker-compose run --rm laravel php artisan test`

### Endpoints

#### Add runner

`POST /api/runners`

Expected input:

```json
{
    "name": "Romero Britto",
    "cpf": "97023992030",
    "birth_date": "1998-01-01"
}
```

Response:

```json
{
    "id": 3,
    "name": "Romero Britto",
    "cpf": "97023992030",
    "birth_date": "1998-01-01"
}
```

#### Add race

`POST /api/races`

Expected input:

```json
{
    "type": "42_KM",
    "date": "2020-12-01"
}
```

Response:

```json
{
    "id": 3,
    "type": "42_KM",
    "date": "2020-12-01"
}
```

#### Add runner to race

`POST /api/races-runners`

Expected input:

```json
{
    "race_id": 6,
    "runner_id": 4
}
```

Response:

```json
{
     "id": 7,
     "race_id": 2,
     "runner_id": 3,
     "started_at": null,
     "finished_at": null
}
```

#### Add runner results

`POST /api/races-runners/{id}/add-results`

Expected input:

```json
{
     "started_at": "2020-11-30 12:32:23",
     "finished_at": "2020-11-30 12:33:22"
}
```

Response:

```json
{
       
     "id": 4,
     "race_id": 2,
     "runner_id": 1,
     "started_at": "2020-11-30 12:32:23+00",
     "finished_at": "2020-11-30 12:33:22+00"
       
}
```

#### List runners

`GET /api/races-runners`

Parameter:

`query: group_by - indicates the list type. IN ['age_type', 'race_type']`

Responses:

If group_by equals 'age_type'

```json
{
    "21_KM": {
        "ABOVE_FIFTY_FIVE": [
            {
                "race_id": 5,
                "race_type": "21_KM",
                "runner_id": 3,
                "runner_age": 117,
                "runner_name": "Wally west",
                "age_type": "ABOVE_FIFTY_FIVE",
                "position": 1
            }
        ],
        "TWENTY_FIVE_TO_THIRTY_FIVE": [
            {
                "race_id": 5,
                "race_type": "21_KM",
                "runner_id": 1,
                "runner_age": 31,
                "runner_name": "Barry Allen",
                "age_type": "TWENTY_FIVE_TO_THIRTY_FIVE",
                "position": 1
            },
            {
                "race_id": 5,
                "race_type": "21_KM",
                "runner_id": 2,
                "runner_age": 33,
                "runner_name": "A-Train",
                "age_type": "TWENTY_FIVE_TO_THIRTY_FIVE",
                "position": 2
            }
        ]
    },
    "42_KM": {
        "ABOVE_FIFTY_FIVE": [
            {
                "race_id": 6,
                "race_type": "42_KM",
                "runner_id": 3,
                "runner_age": 117,
                "runner_name": "Wally west",
                "age_type": "ABOVE_FIFTY_FIVE",
                "position": 1
            }
        ],
        "TWENTY_FIVE_TO_THIRTY_FIVE": [
            {
                "race_id": 6,
                "race_type": "42_KM",
                "runner_id": 1,
                "runner_age": 31,
                "runner_name": "Barry Allen",
                "age_type": "TWENTY_FIVE_TO_THIRTY_FIVE",
                "position": 1
            },
            {
                "race_id": 6,
                "race_type": "42_KM",
                "runner_id": 2,
                "runner_age": 33,
                "runner_name": "A-Train",
                "age_type": "TWENTY_FIVE_TO_THIRTY_FIVE",
                "position": 2
            }
        ]
    }
}
```

If group by equals race_type

```json
{
    "21_KM": [
        {
            "race_id": 5,
            "race_type": "21_KM",
            "runner_id": 1,
            "runner_age": 31,
            "runner_name": "Barry Allen",
            "position": 1
        },
        {
            "race_id": 5,
            "race_type": "21_KM",
            "runner_id": 3,
            "runner_age": 117,
            "runner_name": "Wally west",
            "position": 2
        },
        {
            "race_id": 5,
            "race_type": "21_KM",
            "runner_id": 2,
            "runner_age": 33,
            "runner_name": "A-Train",
            "position": 3
        }
    ],
    "42_KM": [
        {
            "race_id": 6,
            "race_type": "42_KM",
            "runner_id": 1,
            "runner_age": 31,
            "runner_name": "Barry Allen",
            "position": 1
        },
        {
            "race_id": 6,
            "race_type": "42_KM",
            "runner_id": 3,
            "runner_age": 117,
            "runner_name": "Wally west",
            "position": 2
        },
        {
            "race_id": 6,
            "race_type": "42_KM",
            "runner_id": 2,
            "runner_age": 33,
            "runner_name": "A-Train",
            "position": 3
        }
    ]
}
```