<?php

namespace App\Services;

use App\Exceptions\ValidationException;
use App\Models\Runner;
use App\Repositories\RunnerRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class RunnerService
{
    protected const MINIMUM_RUNNER_AGE = 18;
    protected RunnerRepository $runnerRepository;

    public function __construct(RunnerRepository $runnerRepository)
    {
        $this->runnerRepository = $runnerRepository;
    }

    /**
     * Add a new runner.
     *
     * @param array $data Must be an array containing the following keys:
     *   - name: The name of the runner
     *   - cpf: The cpf of the runner
     *   - birth_date: The birth date of the runner
     * @return Runner
     */
    public function store(array $data): Runner
    {
        $validator = $this->getValidator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator->errors()->toArray(), "Could not create runner");
        }

        return $this->runnerRepository->store($data);
    }

    protected function getValidator(array $data): \Illuminate\Contracts\Validation\Validator
    {
        $date = Carbon::now();
        $date->subYears(static::MINIMUM_RUNNER_AGE);

        $beforeThan = $date->format('Y-m-d');

        return Validator::make($data, [
            'name' => 'required|max:255|min:5',
            'cpf' => 'required|unique:runners|cpf',
            'birth_date' => "required|date_format:Y-m-d|before:$beforeThan",
        ], [
            'cpf.cpf' => 'The cpf field is invalid.'
        ]);
    }
}
