<?php

namespace App\Services;

use App\Exceptions\ValidationException;
use App\Models\Race;
use App\Models\RaceRunner;
use App\Models\RaceRunnerByAge;
use App\Models\RaceRunnerByType;
use App\Repositories\RaceRepository;
use App\Repositories\RaceRunnerRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RaceRunnerService
{
    public const GROUP_BY_TYPE = 'race_type';
    public const GROUP_BY_AGE = 'age_type';
    public const GROUP_TYPES = [
        self::GROUP_BY_AGE,
        self::GROUP_BY_TYPE
    ];
    protected RaceRunnerRepository $raceRunnerRepository;
    protected RaceRepository $raceRepository;

    public function __construct(RaceRunnerRepository $raceRunnerRepository, RaceRepository $raceRepository)
    {
        $this->raceRunnerRepository = $raceRunnerRepository;
        $this->raceRepository = $raceRepository;
    }

    /**
     * Add a runner to a race.
     *
     * @param array $data Must be an array with the following keys:
     *   - race_id: The id of the race
     *   - runner_id: The id of the runner
     *
     * @return RaceRunner|null
     */
    public function addRunnerToRace(array $data)
    {
        $integrityValidator = $this->getIntegrityValidator($data);

        if ($integrityValidator->fails()) {
            throw new ValidationException($integrityValidator->errors()->toArray(), "Could not save runner");
        }
        $race = $this->raceRepository->findById($data['race_id']);

        $this->validateRunnerSchedule($race, (int)$data['runner_id']);

        return $this->raceRunnerRepository->save($data);
    }

    /**
     * Get the validator related to data integrity.
     *
     * This is used when creating the race-runner link
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getIntegrityValidator(array $data): \Illuminate\Contracts\Validation\Validator
    {
        $uniqueRule = Rule::unique('races_runners')->where(function ($query) use ($data) {
            $query->where('race_id', $data['race_id'] ?? null);
            $query->where('runner_id', $data['runner_id'] ?? null);
        });

        return Validator::make($data, [
            'race_id' => ['required', 'exists:races,id'],
            'runner_id' => ['required', 'exists:runners,id', $uniqueRule],
        ], [
            'runner_id.unique' => 'The runner already is in this race.'
        ]);
    }

    /**
     * Check that the runner is in not in another race on a date
     *
     * @param Race $race The race we want to link the runner to. We use this race date
     * to search for other races the runner might be participating
     * @param int $runnerId The id of the runner
     */
    protected function validateRunnerSchedule(Race $race, int $runnerId): void
    {
        if ($this->raceRunnerRepository->runnerInAnotherRaceSameDate($race, $runnerId)) {
            throw new ValidationException([
                'runner_id' => ['The runner already has a race on that date.',]
            ], "The runner already has a race on that date.");
        }
    }

    /**
     * Add the runner results to a race.
     *
     * @param int $raceRunnerId The id of the races_runners table
     * @param array $data Must be an array containing the following keys:
     *   - started_at: When the runner started the race
     *   - finished_at: When the runner finished the race
     * @return RaceRunner|null
     */
    public function addRunnerResultsToRace(int $raceRunnerId, array $data)
    {
        /** @var RaceRunner $raceRunner */
        $raceRunner = RaceRunner::query()->findOrFail($raceRunnerId);

        $race = $raceRunner->race;

        $dataValidator = $this->getDataValidator($data, $race);

        if ($dataValidator->fails()) {
            throw new ValidationException($dataValidator->errors()->toArray(), "Could not save runner");
        }

        return $this->raceRunnerRepository->save($data, $raceRunnerId);
    }

    /**
     * Get the validator related to data content.
     *
     * Use this to validate the runner's results dates
     *
     * @param array $data
     * @param Race $race
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getDataValidator(array $data, Race $race): \Illuminate\Contracts\Validation\Validator
    {
        $raceDate = Carbon::parse($race->date)->format('Y-m-d H:i:s');

        return Validator::make($data, [
            'started_at' => "required|date_format:Y-m-d H:i:s|after:$raceDate",
            'finished_at' => 'required|date_format:Y-m-d H:i:s|after:started_at',
        ]);
    }

    /**
     * List runners according to a list type
     *
     * @param string $groupType The type of listing. Must be a valid static::GROUP_TYPE
     * @return array
     */
    public function list(string $groupType): array
    {
        $validator = Validator::make(['group_by' => $groupType], [
            'group_by' => ['required', Rule::in(static::GROUP_TYPES)]
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator->errors()->toArray(), "Invalid group type");
        }

        if ($groupType === static::GROUP_BY_TYPE) {
            return $this->listGroupByType();
        }

        return $this->listGroupByAge();
    }

    /**
     * List runners grouping by race type and age type.
     *
     * Return the runners in ascending order by race time and id. If a category has no results, it will not be in
     * the return data
     * The return layout is:
     * [
     *   'race_type_1' => [
     *      properties
     *   ]
     * ]
     *
     * @return array
     */
    public function listGroupByType(): array
    {
        $result = [];
        $data = $this->raceRunnerRepository->listGroupByType();

        /** @var RaceRunnerByType $row */
        foreach ($data as $row) {
            $result[$row->race_type][] = $row;
        }

        return $result;
    }

    /**
     * List runners grouping by race type and age type.
     *
     * Return the runners in ascending order by race time and id. If a category has no results, it will not be in
     * the return data
     * The return layout is:
     * [
     *   'race_type_1' => [
     *      'age_type_1' => [
     *         [
     *              properties
     *         ]
     *      ]
     *   ]
     * ]
     *
     * @return array
     */
    public function listGroupByAge(): array
    {
        $result = [];
        $data = $this->raceRunnerRepository->listGroupByAge();

        /** @var RaceRunnerByAge $row */
        foreach ($data as $row) {
            $result[$row->race_type][$row->age_type][] = $row;
        }

        return $result;
    }
}
