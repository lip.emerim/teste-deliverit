<?php

namespace App\Services;

use App\Exceptions\ValidationException;
use App\Models\Race;
use App\Repositories\RaceRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RaceService
{
    protected RaceRepository $raceRepository;

    public function __construct(RaceRepository $raceRepository)
    {
        $this->raceRepository = $raceRepository;
    }

    /**
     * Add a new race
     *
     * @param array $data Must be an array containing the following keys.
     *  - type: The type of the race.
     *  - date: The date of the race
     *
     * @return Race
     */
    public function store(array $data): Race
    {
        $validator = $this->getValidator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator->errors()->toArray(), "Could not create race");
        }

        return $this->raceRepository->store($data);
    }

    protected function getValidator(array $data): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($data, [
            'type' => ['required', Rule::in(Race::RACE_TYPES)],
            'date' => "required|date_format:Y-m-d|after_or_equal:today",
        ]);
    }
}
