<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\Services\RaceRunnerService;
use Illuminate\Http\Request;

class RaceRunnerController extends Controller
{
    protected RaceRunnerService $raceRunnerService;

    public function __construct(RaceRunnerService $raceRunnerService)
    {
        $this->raceRunnerService = $raceRunnerService;
    }

    public function addRunnerToRace(Request $request)
    {
        $data = $request->all(['race_id', 'runner_id']);

        try {
            $raceRunner = $this->raceRunnerService->addRunnerToRace($data);

            return response()->json($raceRunner, 201);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => $e->getErrors()
            ], 422);
        }
    }

    public function addRunnerResultsToRace(int $raceRunnerId, Request $request)
    {
        $data = $request->all(['started_at', 'finished_at']);

        try {
            $raceRunner = $this->raceRunnerService->addRunnerResultsToRace($raceRunnerId, $data);

            return response()->json($raceRunner, 200);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => $e->getErrors()
            ], 422);
        }
    }

    public function list(Request $request)
    {
        $groupBy = $request->group_by ?? RaceRunnerService::GROUP_BY_TYPE;

        try {
            return $this->raceRunnerService->list($groupBy);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => $e->getErrors()
            ], 422);
        }
    }
}
