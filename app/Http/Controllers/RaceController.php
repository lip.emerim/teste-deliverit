<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\Services\RaceService;
use Illuminate\Http\Request;

class RaceController extends Controller
{
    protected RaceService $raceService;

    public function __construct(RaceService $raceService)
    {
        $this->raceService = $raceService;
    }

    public function store(Request $request)
    {
        $data = $request->all(['type', 'date']);

        try {
            $race = $this->raceService->store($data);

            return response()->json($race, 201);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => $e->getErrors()
            ], 422);
        }
    }
}
