<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\Services\RunnerService;
use Illuminate\Http\Request;

class RunnerController extends Controller
{
    protected RunnerService $runnerService;

    public function __construct(RunnerService $runnerService)
    {
        $this->runnerService = $runnerService;
    }

    public function store(Request $request)
    {
        $data = $request->all(['name', 'birth_date', 'cpf']);

        try {
            $runner = $this->runnerService->store($data);

            return response()->json($runner, 201);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => $e->getErrors()
            ], 422);
        }
    }
}
