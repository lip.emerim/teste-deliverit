<?php

namespace App\Repositories;

use App\Models\Race;

class RaceRepository
{
    protected Race $race;

    public function __construct(Race $race)
    {
        $this->race = $race;
    }

    public function store($data): Race
    {
        $runner = clone $this->race;
        $runner->fill($data)->save();
        return $runner->fresh();
    }

    public function findById(int $id): Race
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Race::query()->findOrFail($id);
    }
}
