<?php

namespace App\Repositories;

use App\Models\Race;
use App\Models\RaceRunner;
use App\Models\RaceRunnerByAge;
use App\Models\RaceRunnerByType;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RaceRunnerRepository
{
    protected RaceRunner $raceRunner;

    public function __construct(RaceRunner $raceRunner)
    {
        $this->raceRunner = $raceRunner;
    }

    public function save(array $data, ?int $id = null)
    {
        $raceRunner = $this->getModel($id);
        $raceRunner->fill($data)->save();
        return $raceRunner->fresh();
    }

    public function getModel(?int $id): RaceRunner
    {
        if ($id === null) {
            return clone $this->raceRunner;
        }

        /** @var RaceRunner $raceRunner */
        $raceRunner = RaceRunner::query()->findOrFail($id);
        return $raceRunner;
    }

    public function runnerInAnotherRaceSameDate(Race $race, int $runnerId): bool
    {
        return Race::query()
            ->join(
                $this->raceRunner->getTable(),
                static function (JoinClause $join) use ($runnerId) {
                    $join->on('race_id', '=', 'races.id');
                    $join->where('runner_id', '=', $runnerId);
                },
            )
            ->whereDate('date', '=', Carbon::parse($race->date)->format('Y-m-d'))
            ->where('races.id', '!=', $race->id)->exists();
    }

    /**
     * List runners, partitioning the position by race_type.
     *
     * Each row is an object of:
     * @return Collection
     * @see RaceRunnerByType
     *
     */
    public function listGroupByType()
    {
        return DB::table(function (Builder $builder) {
            $this->getBaseListSelect($builder);
        }, 'base_data')->select([
            'race_id',
            'race_type',
            'runner_id',
            'runner_age',
            'runner_name',
            DB::raw('row_number() over (partition by race_type order by race_time, runner_id) AS position')
        ], 'base_data')->get();
    }

    public function getBaseListSelect(Builder $builder): Builder
    {
        return $builder->select([
            'r.id AS race_id',
            'r.type AS race_type',
            'r2.id AS runner_id',
            DB::raw('EXTRACT(year FROM age(current_date, birth_date))::INTEGER AS runner_age'),
            'r2.name AS runner_name',
            DB::raw('finished_at - started_at AS race_time')
        ])->from('races_runners')
            ->join(
                'races AS r',
                'r.id',
                '=',
                'races_runners.race_id'
            )->join(
                'runners AS r2',
                'r2.id',
                '=',
                'races_runners.runner_id'
            )->whereNotNull('races_runners.started_at')
            ->whereNotNull('races_runners.finished_at');
    }

    /**
     * List runners, partitioning the position by age and race_type.
     *
     * Each row is an object of:
     * @return Collection
     * @see RaceRunnerByAge
     *
     */
    public function listGroupByAge()
    {
        return DB::table(function (Builder $builder) {
            $this->getBaseListSelect($builder);
        }, 'base_data')->select([
            'race_id',
            'race_type',
            'runner_id',
            'runner_age',
            'runner_name',
            'r3.name AS age_type',
            DB::raw('row_number() over 
                           (partition by race_type, r3.name order by race_time, runner_id) as position')
        ], 'base_data')->join(
            'runner_ages AS r3',
            static function (JoinClause $join) {
                $join->on('runner_age', '>=', 'r3.start_age');
                $join->on('runner_age', '<=', 'r3.end_age');
            },
        )->get();
    }
}
