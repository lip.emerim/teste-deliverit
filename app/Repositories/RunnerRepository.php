<?php

namespace App\Repositories;

use App\Models\Runner;

class RunnerRepository
{
    protected Runner $runner;

    public function __construct(Runner $runner)
    {
        $this->runner = $runner;
    }

    public function store($data): Runner
    {
        $runner = clone $this->runner;
        $runner->fill($data)->save();
        return $runner->fresh();
    }
}
