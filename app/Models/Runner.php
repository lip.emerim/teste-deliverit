<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Runner
 * @package App\Models
 *
 * @property string $name
 * @property string $cpf
 * @property Carbon $birthDate
 * @property int $id
 */
class Runner extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'cpf',
        'birth_date'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function races()
    {
        return $this->belongsToMany(Race::class)
            ->using(RaceRunner::class)
            ->withPivot(
                ['started_at', 'finished_at']
            )->withTimestamps();
    }
}
