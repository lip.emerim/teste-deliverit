<?php

namespace App\Models;

use App\Models\Enums\RaceTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Race
 * @package App\Models
 *
 * @property string $type
 * @property string $date
 * @property int id
 */
class Race extends Model
{
    use HasFactory;

    public const RACE_TYPES = [
        RaceTypes::THREE_KM,
        RaceTypes::FIVE_KM,
        RaceTypes::TEN_KM,
        RaceTypes::TWENTY_ONE_KM,
        RaceTypes::FORTY_TWO_KM
    ];

    protected $fillable = ['type', 'date'];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function runners()
    {
        return $this->belongsToMany(Runner::class)
            ->using(RaceRunner::class)
            ->withPivot(
                ['started_at', 'finished_at']
            )->withTimestamps();
    }
}
