<?php

namespace App\Models\Enums;

class RaceTypes
{
    public const THREE_KM = '3_KM';
    public const FIVE_KM = '5_KM';
    public const TEN_KM = '10_KM';
    public const TWENTY_ONE_KM = '21_KM';
    public const FORTY_TWO_KM = '42_KM';
}
