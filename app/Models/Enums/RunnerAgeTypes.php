<?php

namespace App\Models\Enums;

class RunnerAgeTypes
{
    public const EIGHTEEN_TO_TWENTY_FIVE = 'EIGHTEEN_TO_TWENTY_FIVE';
    public const TWENTY_FIVE_TO_THIRTY_FIVE = 'TWENTY_FIVE_TO_THIRTY_FIVE';
    public const THIRTY_FIVE_TO_FORTY_FIVE = 'THIRTY_FIVE_TO_FORTY_FIVE';
    public const FORTY_FIVE_TO_FIFTY_FIVE = 'FORTY_FIVE_TO_FIFTY_FIVE';
    public const ABOVE_FIFTY_FIVE = 'ABOVE_FIFTY_FIVE';
}
