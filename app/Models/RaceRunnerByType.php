<?php

namespace App\Models;

use stdClass;

class RaceRunnerByType extends stdClass
{
    public int $race_id;
    public string $race_type;
    public int $runner_id;
    public string $runner_age;
    public string $runner_name;
    public int $position;
}
