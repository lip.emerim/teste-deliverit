<?php

namespace App\Models;

use App\Models\Enums\RunnerAgeTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RunnerAges extends Model
{
    use HasFactory;

    public const RUNNER_AGES = [
        RunnerAgeTypes::EIGHTEEN_TO_TWENTY_FIVE,
        RunnerAgeTypes::TWENTY_FIVE_TO_THIRTY_FIVE,
        RunnerAgeTypes::THIRTY_FIVE_TO_FORTY_FIVE,
        RunnerAgeTypes::FORTY_FIVE_TO_FIFTY_FIVE,
        RunnerAgeTypes::ABOVE_FIFTY_FIVE
    ];
}
