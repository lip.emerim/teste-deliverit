<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class RaceRunner
 * @package App\Models
 *
 * @property int $raceId
 * @property int $runnerId
 * @property Race $race
 * @property Runner $runner
 * @property string $started_at
 * @property string $finished_at
 *
 */
class RaceRunner extends Pivot
{
    public $incrementing = true;
    public $fillable = [
        'race_id',
        'runner_id',
        'started_at',
        'finished_at',
    ];
    public $hidden = [
        'created_at',
        'updated_at',
    ];
    protected $table = 'races_runners';

    public function race()
    {
        return $this->belongsTo(Race::class);
    }

    public function runner()
    {
        return $this->belongsTo(Runner::class);
    }
}
